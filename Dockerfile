FROM python:3.6-jessie

RUN apt-get update && \
    apt-get upgrade -y

ARG NB_UID_ARG
ARG NB_HOST_ARG

ENV NB_USER spark-controller
ENV NB_PORT 8888
ENV NB_UID $NB_UID_ARG
ENV NB_HOST $NB_HOST_ARG

RUN env
RUN adduser --gecos '' --shell /bin/bash --uid $NB_UID --disabled-password $NB_USER
RUN chown -R $NB_USER /home/$NB_USER /usr/local/
USER $NB_USER

WORKDIR /home/$NB_USER

# Download the kubectl binary
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod a+x kubectl && \
    mv kubectl /usr/local/bin/kubectl

# Install python modules for oci, kubernetes, and notebooks
RUN pip install nbgitpuller oci kubernetes \
    git+https://gitlab.com/ali1rathore/pyhelm.git \
    git+https://gitlab.com/SparklineData/spark-on-oci.git
    git+https://gitlab.com/sparkomics/vcf.git
    jupyter jupyter_contrib_nbextensions && \
    jupyter contrib nbextension install --sys-prefix && \
    jupyter nbextension enable execute_time/ExecuteTime --sys-prefix && \
    jupyter serverextension enable --py nbgitpuller --sys-prefix

EXPOSE $NB_PORT
ENV PYTHONDONTWRITEBYTECODE=1
# CMD jupyter-notebook --ip 0.0.0.0 --NotebookApp.custom_display_url="http://${NB_HOST}:${NB_PORT}/git-pull?repo=https%3A%2F%2Fgitlab.com%2Fsparklinedata%2Fspark-on-oci&urlpath=tree%2Fspark-on-oci%2Fnotebooks%2FSpark-Controller.ipynb"
