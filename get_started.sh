#!/bin/bash

# download spark-on-oci
git clone https://gitlab.com/SparklineData/spark-on-oci.git ../spark-on-oci
cd ../spark-on-oci

HOST=0.0.0.0
# or HOST=$(dig +short myip.opendns.com @resolver1.opendns.com)

echo "Building a docker image"

docker build  --build-arg NB_HOST_ARG="${HOST}" \
        --build-arg NB_UID_ARG=$(id -u $USER) \
        --build-arg http_proxy --build-arg HTTPS_PROXY \
        -t spark-controller-template .

# run the spark controller
#./bin/spark-controller.sh
